# Scrut

## Introduction

Scrut is a *Feedly* addon that gives suggestions to the user based on her **reading habit**.

Based on the read articles on Feedly the tool trains a machine learning algorithm which is then used to predict which
unread articles are interesting for the user.

## Usage

See command line arguments:

```shell
python main.py -h
```

## How it works

The application breaks into two separate parts. First is learning a model using the read items of the given user. This
model is then used to evaluate the uread items to tell whether it should be read or not.

### Learning a model

FeedlySuggest uses machine learning to match articles to be read. It uses the following attributes of an item during the
learning process:

``` JavaScript
"item": {
    "origin": {
        "streamId": "feed/http://www.theverge.com/web/rss/index.xml"
    },
    "title": "Will they or won't they: these bots",
    "content": {
        "content": "<p>At the start of 2016, Microsoft's Internet Explorer was still the most
            commonly used browser on the Web; it finished 2015 being used by about 46 percent of
            Web users, with 32 percent preferring Chrome, and 12 percent using Firefox. Windows 10
            just passed <a href=\"http://store.steampowered.com/hwsurvey/\">50 percent</a> of Steam
            users at the end of the year."
    },
    "summary": {
        "content": "A couple of newcomers enter the scene, and Opera changes ownership."
    }
}
```

*Note:* `summary` will be only used when `content` cannot be found.

The learning process consists of the following steps:

1. **Sample data**: Collect entries from Feedly - containing both read and unread items.
2. **Text preparation** – *applies to `title` and `content` only*: Alter the text so that the machine learning 
classifier performance is better.
    1. *Lower-casing*: Text is converted into lower-case
    2. *Stripping HTML*: All HTML tags are removed from the text.
    3. *Normalizing numbers*: All numbers are replaced with the text `number`
    4. *Normalizing dollars*: All dollar signs are replaced with the text `dollar`
    5. *Word stemming*: Words are reduced to their stemmed form using [Snowball](http://snowballstem.org). E.g. 
   `colder`, `coldest`, `coldly`, `coldness` will be replaced with `cold`.
    6. *Removal of non-words*: Non-alphabetic characters are removed (e.g. punctuation, whitespaces)
3. **Vocabulary list**: The algorithm focuses only the most frequently occurring words. The list contains those words 
which occur at least 100 times in the texts.
4. **Word indexing**: All the text in the sample data is being replaced with the corresponding index in the Vocabulary 
list. The words which are not present are going to be removed.
5. **Feature extraction**: The algorithm uses a feature vector of size of the Vocabulary list. For any given sample data
the feature vector's `i`-th element is `1` if the Vocabulary list's `i`-th element is present in its text, `0` 
otherwise.
6. **Training SVM**: Using the preprocessed sample data the training dataset is being used to train the SVM classifier.
7. **Saving the model**: Once the training is complete the model is being saved so that it can be used to predict the
probability of the likeliness of a new entry being interesting

### Prediction

Using the trained model the application is going to predict if any of the unread items in the Feedly subscription is
interesting or not.

The prediction process consists of the following steps:

1. **Sample data**: Retrieve new entries from Feedly.
2. **Text preparation**: The same text modification as above.
3. **Feature extraction**: The same method as above.
4. **Predict**: Using the trained SVM classifier the application suggest entries to read. These are being tagged with
`suggested` so that they can be read using any Feedly application.