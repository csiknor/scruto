# TO DO

This document provides an overview about what is needed to be done during the development. The steps are listed in order
and grouped by major milestones.

## Features

### Prerequisite

* InoReader client

### Data retrieval

* InoReader read items loader
* Local data storage

### Model Training

* Item preparation
* Model training
* Model verification
* Local model storage

### Prediction

* InoReader new items loader
* Model execution
* InoReader tagging
