import logging
from argparse import ArgumentParser

from scrut import scrut
from scrut.loaders.feedlyloader import FeedlyLoader
from scrut.loaders.inoreaderloader import InoreaderLoader


def set_up_logging():
    """
    Configures logging for the application.
    :return: the main Logger
    """
    logging.basicConfig(level=logging.DEBUG)
    return logging.getLogger(__name__)


def load_arguments():
    """
    Defines the arguments of the CLI application
    :return: arguments read from the CLI
    """
    parser = ArgumentParser(description='Scrut helps you find the items you''re interested in')
    parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', default=True,
                        help='verbose output (Default)')
    parser.add_argument('-q', '--quiet', action='store_false', dest='verbose', default=True,
                        help='don\'t print status messages to stdout')
    parser.add_argument('-s', '--source', dest='source', help='source of the items (feedly, inoreader)')
    parser.add_argument('-u', '--username', dest='username', help='Inoreader username for source login')
    parser.add_argument('-p', '--password', dest='password', help='Inoreader password for source login')
    parser.add_argument('-i', '--app_id', dest='app_id', help='Inoreader app_id for app')
    parser.add_argument('-k', '--app_key', dest='app_key', help='Inoreader app_key for app')
    parser.add_argument('-t', '--access_token', dest='access_token', help='Feedly access_token for source login')
    parser.add_argument('-d', '--days', dest='days', help='load data for a number of days')
    parser.add_argument('action', choices=['collect', 'learn', 'predict'],
                        help='action to take (collect|learn|predict)')

    args = parser.parse_args()

    return args


def main():
    """
    The main steps if the module is used to launch the application.
    """
    logger.debug('Starting in command line mode')

    args = load_arguments()

    if args.source == 'feedly':
        loader = FeedlyLoader(access_token=args.access_token, positive_tag='saved')
    elif args.source == 'inoreader':
        loader = InoreaderLoader(args.username, args.password, args.app_id, args.app_key)

    if args.action == 'collect':
        scrut.collect(loader, int(args.days))
    elif args.action == 'learn':
        scrut.learn()
    elif args.action == 'predict':
        scrut.predict(loader)


logger = set_up_logging()

if __name__ == '__main__':
    main()
