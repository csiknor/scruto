import itertools
import logging
import re
from Stemmer import Stemmer
from collections import Counter

import langid
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from sklearn import preprocessing, svm

logger = logging.getLogger(__name__)

def learn(items):
    """
    Trains an SVM classificator using items retrieved from the source. The items are being converted to
    a feature vector using a vocabulary that represent the words which are the most common in the data set.

    :param items: items to be used for the learning
    :return: vocabularies, model - the generated vocabularies for each language and the model learned
    """
    logger.debug('Learning has been started')

    prep_items = _prepare_items(items)

    vocabularies = _generate_vocabularies(prep_items)
    [logger.debug('Vocabulary [%s]: %s', lang_id, vocabularies[lang_id]) for lang_id in vocabularies]

    features = _extract_features(prep_items, vocabularies)
    [logger.debug('Features [%s]: %s', index, features) for index, features in enumerate(features[:2])]

    labels = _extract_labels(prep_items)
    [logger.debug('Labels [%s]: %s', index, label) for index, label in enumerate(labels[:2])]
    logger.debug('Positive: %s', sum(labels))

    [logger.debug('POSITIVE: %s - %s', item['site'], item.get('title'))
     for item in itertools.islice((item for label, item in zip(labels, prep_items) if label), 5)]
    [logger.debug('NEGATIVE: %s - %s', item['site'], item.get('title'))
     for item in itertools.islice((item for label, item in zip(labels, prep_items) if not label), 5)]

    model, scaler = _train_svm(features, labels)

    logger.debug(model)

    return vocabularies, model, scaler


def predict(items, vocabularies, model, scaler):
    """
    Calculates the probability of a new item to be interesting

    :param items: items to be used
    :param vocabularies: vocabularies used by the model
    :param model: the learned model
    :return: predicionts - a list of `1` or `0` for each item
    """
    logger.debug('Prediction has been started')

    prep_items = _prepare_items(items)

    if len(prep_items) > 0:
        features = _extract_features(prep_items, vocabularies)
        [logger.debug('Features [%s]: %s', index, features) for index, features in enumerate(features[:2])]

        predictions = _predict_items(model, scaler, features)
        logger.debug('Predictions: %s', predictions)
        [logger.debug("%s: %s - %s", '>>>' if prediction else '---', item['site'],
                      item.get('title'))
         for item, prediction in zip(prep_items, predictions)]

        return predictions
    else:
        logger.debug('Nothing to predict on')

        return None


def _prepare_items(items):
    """
    Prepares the raw items for further processing. Lower-casing, stripping HTML, normalizing numbers, normalizing
    email addresses, normalizing Twitter handlers, normalizing dollars, removing non-words, identifying language,
    stemming words.

    :param items: list of items, where item has `origin.streamId`, `title` and `content.content` or
    `summary.content` attributes.
    :return: list of items with `lang_id`, `origin`, `words`: list of stems, `read`
    """
    logger.debug('Prepare items')

    stemmers = {'en': Stemmer('english'), 'hu': Stemmer('hungarian')}
    langid.set_languages(stemmers.keys())
    lang_stopwords = {
        'en': set(stopwords.words('english')),
        'hu': set(stopwords.words('hungarian'))
    }
    lang_stopwords['hu'].update(['ha', 'is'])

    for item in items:
        # Lower-casing
        text = item['title'].lower() + ' ' + item['content'].lower()

        # Striping HTML
        text = BeautifulSoup(text, 'html.parser').get_text(' ', strip=True)

        # Normalizing numbers
        text = re.sub(r'\b\d+(?:st|nd|rd|th)?\b', 'number', text)

        # Normalizing URLs
        text = re.sub(r'(http|https)://[^\s]*', 'httpaddr', text)

        # Normalizing email addresses
        text = re.sub(r'[^\s]+@[^\s]+', 'emailaddr', text)

        # Normalizing Twitter handlers
        text = re.sub(r'@[^\s]+', 'twtrhdlr', text)

        # Normalizing dollars
        text = re.sub(r'[$]+', 'dollar', text)

        # Non-words removal
        text = re.sub(r'\W+', ' ', text)

        # Word stemming
        lang_id = langid.classify(text)[0]
        words = filter(lambda word: word not in lang_stopwords[lang_id], text.split())
        stems = stemmers[lang_id].stemWords(words)

        item['lang_id'] = lang_id
        item['words'] = stems

        del (item['content'])

    return items


def _generate_vocabularies(items):
    """
    Generating a vocabularies for each language based on the words in the list of items. The vocabulary for each
    language contains the most common words (NUMBER_OF_COMMON_WORDS) in list.

    :param items: list of items with `lang_id`, `words`: list of stems
    :return: dict of lists by language ID
    """
    logger.debug('Generate vocabulary')

    counters = {'en': Counter(), 'hu': Counter()}

    for item in items:
        counters[item['lang_id']].update(item['words'])

    vocabularies = {lang_id: [word for word, _ in counters[lang_id].most_common(NUMBER_OF_COMMON_WORDS)]
                    for lang_id in counters}

    origins = {item['origin'] for item in items}
    vocabularies['origins'] = sorted(origins)

    return vocabularies


def _extract_features(items, vocabulary):
    """
    Extracts feature lists from each items based on their words and whether those can be found in the vocabulary.
    A feature list is either True or False given the corresponding item in the vocabulary can be found in the
    item's words or not.

    :param items: list of items with `lang_id`, `words`: list of stems
    :param vocabulary: dict of lists by language ID
    :return: list of features which is a list of 1 or 0 values
    """
    logger.debug('Extract features')

    origin_inds = {item: ind for ind, item in enumerate(vocabulary['origins'])}

    features = [
        [origin_inds[item['origin']]] +
        [1 if word in item['words'] else 0 for word in vocabulary[item['lang_id']]]
        for item in items]

    return features


def _extract_labels(items):
    """
    Extracts the labels from each item. The label is if the item has been read or just marked.

    :param items: list of items with `read`
    :return: list of labels: 1 or 0
    """
    logger.debug('Extracting labels')

    labels = [1 if item['read'] else 0 for item in items]

    return labels


def _train_svm(features, labels):
    """
    Trains a model using `scikit-learn` SVM and the input features and labels.

    :param features: a list of features
    :param labels: a list of labels
    :return: the trained model
    """
    logger.debug('Train SVM')

    X, y = features[:int(len(features)*.8)], labels[:int(len(features)*.8)]
    X_test, y_test = features[int(len(features)*.8):], labels[int(len(features)*.8):]

    scaler = preprocessing.StandardScaler().fit(X)

    model = svm.SVC(C=10, verbose=True)
    model.fit(scaler.transform(X), y)

    logger.debug('Score: %s', model.score(scaler.transform(X_test), y_test))

    return model, scaler


def _predict_items(model, scaler, features):
    """
    Calculates the probability of a given feature set to be interesting using the learned model.
    :param model: the learned model
    :param features: a list of features
    :return: a list of prediction - `0` or `1`
    """
    logger.debug('Predicting')

    return model.predict(scaler.transform(features))


NUMBER_OF_COMMON_WORDS = 100