class ItemLoader:
    """
    Generic item loader that provides common features.
    """

    def _extract(self, item, read):
        """
        Extract only the required attributes of any item:
            * `item_id`: the URL to the original item
            * `origin`: the id of the feed
            * `site`: the name of the feed
            * `title`: the title of the item
            * `content`: the content or summary of the item
            * `read`: whether the item has been read or not based on the `positive_tag`
        :param item:
        :return:
        """
        if 'canonical' in item:
            item_id = item['canonical'][0]['href']
        elif 'alternate' in item:
            item_id = item['alternate'][0]['href']
        else:
            raise ValueError('No canonical or alternate href found.')

        if 'content' in item:
            content = item['content']['content']
        elif 'summary' in item:
            content = item['summary']['content']
        else:
            content = ""

        return {
            'item_id': item_id,
            'origin': item['origin']['streamId'],
            'site': item['origin'].get('title'),
            'title': item.get('title', ''),
            'author': item.get('author'),
            'published': item.get('published'),
            'content': content,
            'read': read,
        }
