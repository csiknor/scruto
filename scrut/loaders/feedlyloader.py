"""
Introduces the FeedlyLoader class
"""
import itertools
import logging
from datetime import datetime, timedelta

from feedly.client import FeedlyClient

from scrut.itemloader import ItemLoader

FEEDLY_LOAD_ITEMS_REQUEST_MAX_COUNT = 500


class FeedlyLoader(ItemLoader):
    """
    An ItemLoader that supports Feedly to load data from.
    """
    def __init__(self, access_token, positive_tag='read'):
        self.logger = logging.getLogger(FeedlyLoader.__name__)

        self.access_token = access_token
        self.positive_tag = positive_tag
        self.feedly = FeedlyClient(sandbox=False)
        self.profile = self.feedly.get_user_profile(self.access_token)

    def load_old_items(self, max_count=1000, max_days=None):
        """
        Loads all the old items from Feedly for the current user. Returns a list of items both read and marked.
        :param max_count: maximum number of items to be loaded
        :param max_days: maximum age of items to be loaded
        :return: a list of items stripped to the minimal attributes required
        """
        self.logger.debug('Loading old items')

        days_timestamp = None if max_days is None else int(
            (datetime.utcnow() - timedelta(days=max_days)).timestamp() * 1000)

        old_items = filter(lambda item: not item.get('unread', False), self._item_loader(newer_than=days_timestamp))

        if max_count is not None:
            old_items = itertools.islice(old_items, max_count)

        return list(map(self._extract, old_items))

    def load_new_items(self):
        """
        Loads unread items from Feedly.
        :return: a list of items stripped to the minimal attributes required
        """
        self.logger.debug('Load new items')

        items = list(self._item_loader(unread_only=True))

        return list(map(self._extract, items))

    def _extract(self, item):
        return super(FeedlyLoader, self)._extract()._extract(item, any(
                tag['id'] == 'user/{}/tag/global.{}'.format(self.profile['id'], self.positive_tag) for tag in
                item.get('tags', [])))

    def _item_loader(self, unread_only=None, newer_than=None):
        """
        A generator that loads all items from Feedly by FEEDLY_LOAD_ITEMS_REQUEST_MAX_COUNT at a time
        :param unread_only: loads new items only
        :param newer_than: loads item newer than this timestamp
        :return a generator of matching items
        """
        continuation = None
        while True:
            response = self.feedly.get_feed_content(
                self.access_token,
                'user/{}/category/global.all'.format(self.profile['id']),
                count=FEEDLY_LOAD_ITEMS_REQUEST_MAX_COUNT,
                continuation=continuation,
                unreadOnly=unread_only,
                newerThan=newer_than
            )
            for item in response.get('items', []):
                yield item
            continuation = response.get('continuation')
            if continuation is None:
                break
