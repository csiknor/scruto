import itertools
import logging
from datetime import datetime, timedelta

from librssreader.inoreader import RssReader, ClientAuthMethod, ReaderBasicConfig
from toolz import assoc

from scrut.itemloader import ItemLoader

INOREADER_LOAD_ITEMS_REQUEST_MAX_COUNT = 1000


class InoreaderLoader(ItemLoader):
    """
    An ItemLoader that supports Inoreader service.
    """

    def __init__(self, username, password, app_id, app_key):
        self.logger = logging.getLogger(__name__)

        # FIXME we should be using OAuth2
        # auth = OAuth2Method(InoreaderLoader.CLIENT_ID, InoreaderLoader.CLIENT_SECRET)
        auth = ClientAuthMethod(username, password, app_id, app_key)
        self.reader = RssReader(auth)
        self.reader.buildSubscriptionList()
        self.reader.makeSpecialFeeds()
        self.reading_list_feed = self.reader.getSpecialFeed(ReaderBasicConfig.READING_LIST)
        self.recently_read_feed = self.reader.getSpecialFeed(ReaderBasicConfig.RECENTLY_READ_LIST)
        self.read_feed = self.reader.getSpecialFeed(ReaderBasicConfig.READ_LIST)

    def load_old_items(self, max_count=100000, max_days=None):
        """
        Loads old items from the service and determines if the item is read by using the 'recently read' list.

        :param max_count: maximum number of items loaded from the service
        :param max_days: number of days worth of items loaded from the service
        :return: list of old items
        """
        since_timestamp = None if max_days is None else int((datetime.utcnow() - timedelta(days=max_days)).timestamp())

        # all the read items
        read_items = self._item_loader(self.read_feed, read_only=True, newer_than=since_timestamp)

        # recent items that were opened
        recently_read_ids = [item['id'] for item in
                             self._item_loader(self.recently_read_feed, newer_than=since_timestamp)]

        # all the read items marked as read=True if opened
        old_items = map(lambda item: assoc(item, 'read', item['id'] in recently_read_ids), read_items)

        # limited to max_count
        if max_count is not None:
            old_items = itertools.islice(old_items, max_count)

        return list(map(self._extract, old_items))

    def load_new_items(self):
        """
        Loads new items from the service.
        :return: list of new items
        """
        return list(map(self._extract, self._item_loader(self.reading_list_feed, unread_only=True)))

    def _item_loader(self, feed, read_only=None, unread_only=None, newer_than=None):
        """
        A generator that loads all items from InoReader by INOREADER_LOAD_ITEMS_REQUEST_MAX_COUNT at a time
        :param unread_only: loads new items only
        :param newer_than: loads item newer than this timestamp
        :return a generator of matching items
        """
        continuation = None
        while True:
            response = self.reader.getFeedContent(
                feed,
                excludeRead=unread_only,
                continuation=continuation,
                loadLimit=INOREADER_LOAD_ITEMS_REQUEST_MAX_COUNT,
                since=newer_than
            )
            for item in response.get('items', []):
                if not read_only or 'user/{}/state/com.google/read'.format(self.reader.userId) in item['categories']:
                    yield item
            continuation = response.get('continuation')
            if continuation is None:
                break

    def _extract(self, item):
        """
        Extracts generic structure by identifying read items as items with read=True attribute.
        :param item:
        :return:
        """
        return super(InoreaderLoader, self)._extract(item, item.get('read', False))

    def __str__(self):
        return 'InoreaderLoader {}'.format(self.reader.auth.username)
