# -*- encoding: utf-8 -*-

"""
Scrut is an extension to feed aggregator services like Feedly or Inoreader that uses machine learning to suggest
interesting articles from the unread items based on the read items.

It trains an SVM classificator using items retrieved from the source. The classifier is then used to predict the
likeliness of interestingness of a new item in the feed. Those which are interesting are being tagged with
`suggested`.

Requires: python-feedly, beautifulsoup4, PyStemmer, langid, nltk, scikit-learn, librssreader, toolz
"""
import json
import logging
import os
import pickle
from datetime import datetime
from operator import itemgetter
from pathlib import Path

from scrut import candidate

logger = logging.getLogger(__name__)


def _collect_data(loader, max_days):
    """
    Collects the data provided by the loader.

    :param loader: an ItemLoader to load data
    :return: list of items
    """
    items = loader.load_old_items(max_days=max_days)
    logger.debug('Number of old items: %s', len(items))

    return items


def _collect_new_data(loader):
    """
    Collects new data provided by the loader.

    :param loader: an ItemLoader to load data
    :return: list of items
    """
    items = loader.load_new_items()
    logger.debug('Number of new items: %s', len(items))

    return items


def _save_to_disk(items, filename='items.json'):
    # TODO replace hardcoded file
    """
    Persisting the items to disk in JSON. File name: `items.json`. Backs up existing file.
    :param items: list of items
    :return:
    """
    logger.debug('Save %s items into %s', len(items), filename)

    _backup_file(filename)

    with open(filename, 'w') as output_file:
        json.dump(items, output_file, indent=4)


def _backup_file(filename):
    file = Path(filename)

    if file.exists():
        backup_filename = file.stem + ' ' + datetime.fromtimestamp(file.stat().st_ctime).strftime(
            '%Y-%m-%d %H-%M-%S') + file.suffix
        logger.info('File already exists, renaming to `%s`', backup_filename)
        file.rename(backup_filename)


def _load_from_disk(filename='items.json'):
    # TODO replace hardcoded file
    """
    Loads items from a JSON file on the disk. File name: `items.json`
    :return: list of items
    """
    logger.debug('Load items')

    if os.path.exists(filename):
        with open(filename, 'r') as input_file:
            items = json.load(input_file)
    else:
        items = []

    return items


def _save_model(vocabularies, model, scaler):
    """
    Saves the vocabularies and the learned model to a JSON file on the disk. File name: `vocabularies.json`. Backs up
    existing files.
    :param vocabularies: dict of lists by language ID
    :param model: the learned model
    """
    # TODO replace hardcoded file
    voc_filename = 'vocabularies.json'
    mod_filename = 'model.p'

    _backup_file(voc_filename)
    _backup_file(mod_filename)

    with open(voc_filename, 'w') as output_file:
        json.dump(vocabularies, output_file, indent=4)

    with open(mod_filename, 'wb') as output_file:
        pickle.dump(model, output_file)
        pickle.dump(scaler, output_file)


def _load_model():
    """
    Loads the vocabularies and the learned model from a JSON file on the disk. File name: `vocabularies.json`
    :return: vocabularies, model -  dict of lists by language ID; the learned model
    """
    with open('vocabularies.json', 'r') as input_file:
        vocabularies = json.load(input_file)

    with open('model.p', 'rb') as input_file:
        model = pickle.load(input_file)
        scaler = pickle.load(input_file)

    return vocabularies, model, scaler


def collect(loader, max_days):
    """
    Collects the historical data from the source and saves them by appending to the already existing ones.
    :param loader: source of the items
    :param max_days: number of days to look back
    """
    logger.info('Collecting started from %s for %s days', loader, max_days)

    collected_items = _collect_data(loader, max_days)
    logger.info('Collected %s items of which %s is read', len(collected_items),
                len(list(filter(itemgetter('read'), collected_items))))

    _save_items(collected_items, 'items.json')


def _save_items(collected_items, filename):
    stored_items = _load_from_disk(filename)
    logger.info('Loaded %s items from disk', len(stored_items))

    stored_ids = list(map(itemgetter('item_id'), stored_items))
    new_items = [item for item in collected_items if item['item_id'] not in stored_ids]

    if len(new_items) > 0:
        logger.info('Storing %s new items of which %s is read', len(new_items),
                    len(list(filter(itemgetter('read'), new_items))))
        _save_to_disk(new_items + stored_items, filename)
    else:
        logger.info('No new items found')


def learn():
    """
    Using the stored data set of items trains the model.
    """
    items = _load_from_disk()
    vocabularies, model, scaler = candidate.learn(items)
    _save_model(vocabularies, model, scaler)


def predict(loader):
    """
    Collects unread items from the source and predicts interesting articles using the stored model.
    :param loader: source of the new items
    """
    items = _collect_new_data(loader)
    vocabularies, model, scaler = _load_model()
    predictions = candidate.predict(items, vocabularies, model, scaler)
    # TODO write predictions back to service
