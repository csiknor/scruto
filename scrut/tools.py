def merge_no_repeat(iter1, iter2, keyfunc=lambda x: x, combfunc=lambda x, y: x):
    """
    Iterates through the iterators in parallel and combines values if item is found in both iterators based on the key.
    Assumes that the second iterator is a subset of the first one and yields the items in the same order.

    :param iter1: main iterator to apply the changes on
    :param iter2: additional iterator to match the items
    :param keyfunc: function to be used to identify an item
    :param combfunc: function to combine items from main and additional iterator
    :return:
    """
    ref = next(iter1, None)
    for elem in iter2:
        key_elem = keyfunc(elem)  # caching value so we won't compute it for each value in iter1 that is before this one
        while ref is not None and key_elem != keyfunc(ref):
            # Catch up with low values from iter1
            yield ref
            ref = next(iter1, None)
        if ref is not None and key_elem == keyfunc(ref):
            ref = combfunc(ref, elem)
    if ref is not None:
        yield ref
    for elem in iter1:
        # Use remaining items of iter1 if needed
        yield elem
